FROM node:latest

WORKDIR /code
COPY package*.json ./
RUN npm install
COPY ./src ./src
COPY ./public ./public

EXPOSE 3000