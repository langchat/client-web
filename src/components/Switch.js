import React from 'react'
import { Route, Switch as BaseSwitch } from 'react-router-dom';
import Article from '../components/Article';
import Editor from '../components/Editor';
import Home from '../components/Home';
import Chat from '../components/Chat'
import Login from '../components/Login';
import Profile from '../components/Profile';
import ProfileFavorites from '../components/ProfileFavorites';
import Register from '../components/Register';
import Settings from './Settings/Settings';
import { store } from '../store';
import EditProfile from './EditProfile';

const Switch = props => (
  <BaseSwitch>
    <Route exact path="/" component={Home} />
    <Route path="/chat" component={Chat} />
    <Route path="/profile/edit" component={EditProfile} />
    <Route path="/register" component={Register} />
    <Route path="/editor/:slug" component={Editor} />
    <Route path="/editor" component={Editor} />
    <Route path="/article/:id" component={Article} />
    <Route path="/settings" component={Settings} />
    <Route path="/@:username/favorites" component={ProfileFavorites} />
    <Route path="/@:username" component={Profile} />
  </BaseSwitch>
)

export default Switch
