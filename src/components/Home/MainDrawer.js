import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from 'material-ui/styles';
import Drawer from 'material-ui/Drawer';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import { List, ListItem, ListItemText } from 'material-ui';
import { MenuItem } from 'material-ui/Menu';
import TextField from 'material-ui/TextField';
import Typography from 'material-ui/Typography';
import Divider from 'material-ui/Divider';
import { Avatar } from 'langchat-client';
import { Link } from 'react-router-dom';
import LoggedInView from '../Header/LoggedInView'
import modules from '../../modules'

const mapStateToProps = state => ({
  ...state,
  currentUser: state.common.currentUser,
  currentProfile: state.common.currentProfile,
});

const mapDispatchToProps = dispatch => ({
  navigate: msg => dispatch({ type: ACTION.MESSAGE_ADDED, msg }),
});

const drawerWidth = 240;
const styles = theme => ({
  appFrame: {
    position: 'relative',
    display: 'flex',
    width: '100%',
    height: '100%',
  },
  appBar: {
    position: 'absolute',
    width: `calc(100% - ${drawerWidth}px)`,
  },
  'appBar-left': {
    marginLeft: drawerWidth,
  },
  drawerPaper: {
    position: 'relative',
    height: '100%',
    width: drawerWidth,
  },
  drawerHeader: theme.mixins.toolbar,
});

const MainDrawer = (props) => {
  const { classes } = props;

  return (
    <Drawer
      type="permanent"
      classes={{
          paper: classes.drawerPaper,
        }}
      anchor="left"
    >
      <div className={classes.drawerHeader}>
        { props.currentProfile && <LoggedInView /> }
      </div>
      <Divider />
      <List>
        { modules.map(module => (
          <ListItem
            component={Link}
            to={`/home/${module.slug}`}
            button selected
          >
            <ListItemText primary={module.title} />
          </ListItem>
        )) }
        <Divider />
        <ListItem button>
          <ListItemText primary="Trash" />
        </ListItem>
        <ListItem button component="a" href="#simple-list">
          <ListItemText primary="Spam" />
        </ListItem>
      </List>
    </Drawer>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(MainDrawer))
