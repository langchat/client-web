import React from 'react'
import { connect } from 'react-redux';
import { withStyles } from 'material-ui/styles';
import MainDrawer from './MainDrawer'
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import modules from '../../modules'

const mapStateToProps = state => ({
  ...state,
  currentUser: state.common.currentUser,
});

const mapDispatchToProps = dispatch => ({
  navigate: msg => dispatch({ type: ACTION.MESSAGE_ADDED, msg }),
});

const drawerWidth = 240;
const styles = theme => ({
  root: {
    width: '100%',
    height: '100%',
    marginTop: 0,
    zIndex: 1,
    overflow: 'hidden',
  },
  appFrame: {
    position: 'relative',
    display: 'flex',
    width: '100%',
    height: '100%',
  },
  appBar: {
    position: 'absolute',
    width: `calc(100% - ${drawerWidth}px)`,
  },
  content: {
    backgroundColor: theme.palette.background.default,
    width: '100%',
    padding: 0,
    height: '100%',
    marginTop: 0,
    //[theme.breakpoints.up('sm')]: {
    //  height: 'calc(100% - 45px)',
    //  marginTop: theme.spacing.unit,
    //},
  },
});

const Home = ({ classes, match }) => (
  <div className={classes.root}>
    <div className={classes.appFrame}>
      <MainDrawer />
      <main className={classes.content}>
        { modules.map(module => <Route path={`${match.path}/${module.slug}`} component={module.component} />)}
      </main>
    </div>
  </div>)

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Home))
