import { Link } from 'react-router-dom';
import ListErrors from './ListErrors';
import React from 'react';
import { agent } from 'langchat-client'
import { connect } from 'react-redux';

import {
  UPDATE_FIELD_AUTH,
  LOGIN,
  LOGIN_PAGE_UNLOADED, LOAD_MY_PROFILE,
} from '../constants/actionTypes';
import { Button, Grid, Paper, TextField, Typography, Divider } from 'material-ui';

const mapStateToProps = state => ({ ...state.auth });

const mapDispatchToProps = dispatch => ({
  onChangeEmail: value =>
    dispatch({ type: UPDATE_FIELD_AUTH, key: 'email', value }),
  onChangePassword: value =>
    dispatch({ type: UPDATE_FIELD_AUTH, key: 'password', value }),
  onSubmit: (email, password) =>
    dispatch({ type: LOGIN, payload: agent.Auth.login(email, password) }),
  onUnload: () =>
    dispatch({ type: LOGIN_PAGE_UNLOADED }),
  loginFacebook: token => dispatch({ type: LOGIN, payload: agent.Auth.loginFacebook(token) }),
});

class Login extends React.Component {
  constructor() {
    super();
    this.changeEmail = ev => this.props.onChangeEmail(ev.target.value);
    this.changePassword = ev => this.props.onChangePassword(ev.target.value);
    this.submitForm = (email, password) => (ev) => {
      ev.preventDefault();
      this.props.onSubmit(email, password);
    };
  }

  componentDidMount() {
    window.fbAsyncInit = () => {
      FB.init({
        appId: '597362390611859',
        cookie: true,
        xfbml: true,
        version: 'v2.1',
      });

      FB.getLoginStatus((response) => {
        this.statusChangeCallback(response);
      });

      FB.AppEvents.logPageView()
    }

    (function (d, s, id) {
      let js,
        fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) { return; }
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  statusChangeCallback(response) {
    if (response.status === 'connected') {
      this.props.loginFacebook(response.authResponse.accessToken)
    }
  }

  render() {
    const { email, password } = this.props;

    const handleFacbookLogin = () => {
      window.FB.login((res) => {
        window.FB.getLoginStatus((response) => {
          this.statusChangeCallback(response);
        })
      }, { scope: ['email', 'public_profile', 'user_birthday', 'user_friends'] });
    }

    return (
      <Grid container justify="center">
        <Grid item sm={4}>
          <Paper style={{ padding: 16 }}>
            <Typography type="headline" component="h3">
            Sign In
            </Typography>
            <Link to="/register">
            Need an account?
            </Link>

            <ListErrors errors={this.props.errors} />
            <form onSubmit={this.submitForm(email, password)}>
              <TextField
                label="Email or username"
                value={email}
                fullWidth
                onChange={this.changeEmail}
              />
              <TextField
                type="password"
                label="Password"
                value={password}
                fullWidth
                onChange={this.changePassword}
              />
              <Divider />
              <Button
                raised
                type="submit"
                color="primary"
                disabled={this.props.inProgress}
              >
            Sign in
              </Button>
              <Button
                raised
                color="primary"
                disabled={this.props.inProgress}
                onClick={handleFacbookLogin}
              >
            Sign in with Facebook
              </Button>
            </form>
          </Paper>
        </Grid>
      </Grid>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
