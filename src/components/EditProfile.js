import ThreadList from './Chat/ThreadList';
import React from 'react';
import { Link } from 'react-router-dom';
import { agent } from 'langchat-client'
import { connect } from 'react-redux';
import {
  FOLLOW_USER,
  UNFOLLOW_USER,
  PROFILE_PAGE_LOADED,
  PROFILE_PAGE_UNLOADED
} from '../constants/actionTypes';
import {Grid, Paper, TextField, Typography} from "material-ui";

const mapStateToProps = state => ({
  ...state.threadList,
  currentUser: state.common.currentUser,
  profile: state.profile
});

const mapDispatchToProps = dispatch => ({
  onLoad: payload => dispatch({ type: PROFILE_PAGE_LOADED, payload }),
  onUnload: () => dispatch({ type: PROFILE_PAGE_UNLOADED })
});

class EditProfile extends React.Component {
  componentWillMount() {
    //this.props.onLoad(Promise.all([
    //  agent.Profile.get(this.props.match.params.username),
    //]));
  }

  componentWillUnmount() {
    //this.props.onUnload();
  }

  render() {
    const profile = this.props.profile;
    if (!profile) {
      return null;
    }

    const isUser = this.props.currentUser &&
      this.props.profile.username === this.props.currentUser.username;

    return (
      <Grid container justify="center">
        <Grid sm={6} item>
          <Paper style={{ padding: 16 }}>
            <Typography type="headline" component="h3">
              Edit Profile
            </Typography>
            <Grid container>
              <Grid sm={6} item>
                <TextField id="username" label="Username" fullWidth />
              </Grid>
              <Grid sm={6} item>
                <TextField id="email" label="Email" fullWidth />
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
