import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import { agent } from 'langchat-client';
import Home from './Home'
import Login from '../components/Login';
import auth from '../auth'
import Header from './Header/Header';
import { APP_LOAD, LOAD_MY_PROFILE, REDIRECT } from '../constants/actionTypes';
import { store } from '../store';

const mapStateToProps = state => ({
  appLoaded: state.common.appLoaded,
  appName: state.common.appName,
  currentUser: state.common.currentUser,
  redirectTo: state.common.redirectTo,
});

const mapDispatchToProps = dispatch => ({
  onLoad: (payload, token) =>
    dispatch({
      type: APP_LOAD, payload, token, skipTracking: true,
    }),
  onRedirect: () =>
    dispatch({ type: REDIRECT }),
});

const styles = {
  root: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
}

class App extends React.Component {
  componentWillMount() {
    const token = window.localStorage.getItem('jwt');
    if (token) {
      auth.setToken(token);
    }

    this.props.onLoad(token ? agent.Auth.current() : null, token);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.redirectTo) {
      // this.context.router.replace(nextProps.redirectTo);
      store.dispatch(push(nextProps.redirectTo));
      this.props.onRedirect();
    }
  }

  render() {
    console.log(this.props.match.url)
    if (this.props.appLoaded) {
      return (
        <div style={styles.root}>
          <Route path="/home" component={Home} />
          <Route path="/login" component={Login} />
        </div>
      );
    }
    return (
      <div />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
