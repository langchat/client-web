import React from 'react';
import {
  Button, ExpansionPanel, ExpansionPanelActions, ExpansionPanelDetails, ExpansionPanelSummary, FormControl, Grid, Input,
  InputLabel, MenuItem, Select,
  TextField,
  Typography, withStyles,
  Checkbox, ListItemText,
  RaisedButton,
} from 'material-ui';
import { agent, Avatar } from 'langchat-client'
import { ExpandMore } from 'material-ui-icons';
import ListErrors from '../ListErrors';
import { connect } from 'react-redux';
import { SETTINGS_PAGE_UNLOADED, SETTINGS_SAVED, LOAD_MY_PROFILE } from '../../constants/actionTypes';

const languages = require('../../constants/languages')

const styles = theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  icon: {
    verticalAlign: 'bottom',
    height: 20,
    width: 20,
  },
  details: {
    alignItems: 'center',
  },
  column: {
    flexBasis: '33.3%',
  },
  helper: {
    borderLeft: `2px solid ${theme.palette.text.lightDivider}`,
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
  },
  link: {
    color: theme.palette.primary[500],
    textDecoration: 'none',
    '&:hover': {
      textDecoration: 'underline',
    },
  },
});

const mapStateToProps = state => ({
  ...state.settings,
  currentUser: state.common.currentUser,
  currentProfile: state.common.currentProfile,
});

const mapDispatchToProps = dispatch => ({
  onSubmitForm: user =>
    dispatch({ type: SETTINGS_SAVED, payload: agent.Auth.save(user).then(dispatch({ type: LOAD_MY_PROFILE, payload: agent.Profile.me() })) }),
  onUnload: () => dispatch({ type: SETTINGS_PAGE_UNLOADED }),
});

class BasicProfile extends React.Component {
  constructor() {
    super();

    this.state = {
      image: '',
      username: '',
      bio: '',
      email: '',
      native: '',
      languages: [],
      gender: '',
      file: '',
    };

    this.updateState = field => (ev) => {
      if (field === 'languages') {
        console.log(ev.target)
        this.setState(Object.assign({}, this.state, { [field]: ev.target.value }));
      } else {
        this.setState(Object.assign({}, this.state, { [field]: ev.target.value }));
      }
    };

    this.handleFileUpload = ev => {
      const file = ev.target.files[0]
      agent.Profile.setAvatar(file)
    }

    this.submitForm = (ev) => {
      ev.preventDefault();
      const user = Object.assign({}, this.state);
      this.props.onSubmitForm(user);
    };
  }

  componentWillMount() {
    if (this.props.currentUser) {
      Object.assign(this.state, {
        image: this.props.currentUser.image || '',
        username: this.props.currentUser.username,
        bio: this.props.currentUser.bio,
        email: this.props.currentUser.email,
        native: this.props.currentProfile.native,
        languages: this.props.currentProfile.languages,
        gender: this.props.currentProfile.gender,
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currentUser) {
      this.setState(Object.assign({}, this.state, {
        image: nextProps.currentUser.image || '',
        username: nextProps.currentUser.username,
        bio: nextProps.currentUser.bio,
        email: nextProps.currentUser.email,
        native: nextProps.currentProfile.native,
        languages: nextProps.currentProfile.languages,
      }));
    }
  }

  render() {
    const props = this.props
    const { classes } = this.props
    return (
      <Grid container justify="center">
        <Grid sm={6} item>
          <form onSubmit={this.submitForm}>
            <ExpansionPanel defaultExpanded>
              <ExpansionPanelSummary expandIcon={<ExpandMore />}>
                <div className={classes.column}>
                  <Typography className={classes.heading}>Basic</Typography>
                </div>
                <div className={classes.column}>
                  <Typography className={classes.secondaryHeading}>Edit basic profile</Typography>
                </div>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails className={classes.detail}>
                <ListErrors errors={this.props.errors} />
                <Grid container>
                  <Grid sm={6} item>
                    <TextField
                      id="username"
                      label="Username"
                      fullWidth
                      value={this.state.username}
                      onChange={this.updateState('username')}
                    />
                  </Grid>
                  <Grid sm={6} item>
                    <TextField
                      id="email"
                      label="Email"
                      fullWidth
                      value={this.state.email}
                      onChange={this.updateState('email')}
                    />
                  </Grid>
                  <Grid sm={6} item>
                    <FormControl fullWidth>
                      <InputLabel>Native Language</InputLabel>
                      <Select
                        onChange={this.updateState('native')}
                        value={this.state.native || 'none'}
                        input={<Input name="native" />}
                      >
                        {Object.keys(languages).map(lang =>
                        <MenuItem key={lang} value={lang}>{languages[lang]}</MenuItem>)}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid sm={6} item>
                    <FormControl fullWidth>
                      <InputLabel>Studying Language(s)</InputLabel>
                      <Select
                        onChange={this.updateState('languages')}
                        multiple
                        value={this.state.languages}
                        input={<Input name="languages" />}
                        renderValue={selected => selected.map(lang => languages[lang]).join(', ')}
                      >
                        {Object.keys(languages).map((lang, id) =>
                        (<MenuItem key={lang} value={lang}>
                          <Checkbox checked={this.state.languages.includes(lang)} />
                          <ListItemText primary={languages[lang]} />
                         </MenuItem>))}
                      </Select>
                    </FormControl>
                  </Grid>

                  <Grid sm={6} item>
                    <FormControl fullWidth>
                      <InputLabel>Gender</InputLabel>
                      <Select
                        onChange={this.updateState('gender')}
                        value={this.state.gender || 'none'}
                        input={<Input name="gender" />}
                      >
                        <MenuItem value="none">Unspecified</MenuItem>
                        <MenuItem value="male">Male</MenuItem>
                        <MenuItem value="female">Female</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>

                  <Grid sm={6} item>
                    <Avatar user={this.props.currentUser} />
                    <FormControl fullWidth>
                      <Button>
                        <input type="file" onChange={this.handleFileUpload} />
                      </Button>
                    </FormControl>
                  </Grid>
                </Grid>
              </ExpansionPanelDetails>
              <ExpansionPanelActions>
                <Button dense color="primary" type="submit" disabled={this.state.inProgress}>Update</Button>
              </ExpansionPanelActions>
            </ExpansionPanel>
          </form>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(BasicProfile));
