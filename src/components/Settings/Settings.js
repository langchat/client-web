import React from 'react';
import { agent } from 'langchat-client'
import { connect } from 'react-redux';
import {
  SETTINGS_SAVED,
  SETTINGS_PAGE_UNLOADED
} from '../../constants/actionTypes';
import BasicProfile from "./BasicProfile";


const mapStateToProps = state => ({
  ...state.settings,
  currentUser: state.common.currentUser
});

const mapDispatchToProps = dispatch => ({
  onSubmitForm: user =>
    dispatch({ type: SETTINGS_SAVED, payload: agent.Auth.save(user) }),
  onUnload: () => dispatch({ type: SETTINGS_PAGE_UNLOADED })
});

class Settings extends React.Component {
  render() {
    return (
      <div className="settings-page">
        <BasicProfile />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
