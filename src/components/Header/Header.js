import React from 'react';
import { Link } from 'react-router-dom';
import {AppBar, Toolbar, Typography, Button} from "material-ui";
import {connect} from "react-redux";
import {CHANGE_TAB, CHANGE_CURRENT_LANGUAGE} from "../../constants/actionTypes";
import LoggedInView from './LoggedInView'

const LoggedOutView = props => {
  if (!props.currentUser) {
    return [
      <Link key="home" to="/">
        <Button color="contrast">Home</Button>
      </Link>,
      <Link key="login" to="/login">
        <Button color="contrast">Login</Button>
      </Link>,
      <Link key="register" to="/register">
        <Button color="contrast">Register</Button>
      </Link>
    ];
  }
  return null;
};

const mapStateToProps = state => ({
  ...state,
  currentUser: state.common.currentUser,
  currentProfile: state.common.currentProfile,
  currentLanguage: state.common.currentLanguage,
  token: state.common.currentProfile
});

const mapDispatchToProps = dispatch => ({
  changeLanguage: language => dispatch({ type: CHANGE_CURRENT_LANGUAGE, language })
});

class Header extends React.Component {
  render() {
    return (
      <AppBar>
        <Toolbar>
          <Typography key="title" type="title" color="inherit" style={{flex: 1}}>
            <Link key="home" to="/" color="contrast">{this.props.appName}</Link>,
          </Typography>
          { this.props.currentUser && <LoggedInView /> }
          { !this.props.currentUser && LoggedOutView(this.props) }
        </Toolbar>
      </AppBar>
    );
  }
}

const styles = {
  root: {
    borderBottom: '1px solid rgba(0, 0, 0, 0.1)'
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
