import React from "react";
import {Button, IconButton, Input, ListItemIcon, ListItemText, Menu, MenuItem, Select} from "material-ui";
import {AccountCircle, Settings} from "material-ui-icons";
import {withRouter} from "react-router";
import { history } from '../../store'
import {connect} from "react-redux";
import {CHANGE_CURRENT_LANGUAGE, LOGOUT} from "../../constants/actionTypes";
import Avatar from "material-ui/Avatar/Avatar";
import List from "material-ui/List/List";
import ListItemAvatar from "material-ui/List/ListItemAvatar";
import ListItem from "../../../../share/node_modules/material-ui/List/ListItem";
import Divider from "../../../../share/node_modules/material-ui/Divider/Divider";

const languages = require('../../constants/languages')

const mapStateToProps = state => ({
  ...state,
  currentUser: state.common.currentUser,
  currentProfile: state.common.currentProfile,
  currentLanguage: state.common.currentLanguage,
});

const mapDispatchToProps = dispatch => ({
  changeLanguage: language => dispatch({ type: CHANGE_CURRENT_LANGUAGE, language }),
  logOut: () => dispatch({ type: LOGOUT }),
});

class LoggedInView extends React.Component {
  constructor() {
    super()
    this.state = { anchorEl: null }
  }

  render() {
    const { props } = this;
    const { anchorEl } = this.state;
    const open = Boolean(anchorEl);

    const handleMenu = (event) => {
      this.setState({ anchorEl: event.currentTarget })
    }
  
    const handleChangeLanguage = (e) => {
      this.props.changeLanguage(e.target.value)
    }
  
    const handleClose = (route) => {
      this.setState({ anchorEl: null })
      if (route) history.push(route)
    }

    return (
      <React.Fragment>
        <List>
          <ListItem>
          { props.currentProfile && <Avatar src={props.currentProfile.avatar} /> }
          <ListItemText primary={props.currentProfile.username} />
          <Menu
            id="menu-appbar"
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            open={open}
            onClose={handleClose}
          >
            <MenuItem onClick={() => { handleClose(`/@${props.currentUser.username}`) }}>Profile</MenuItem>
            <MenuItem onClick={() => { handleClose('/settings') }}>
              <ListItemIcon><Settings/></ListItemIcon>
              <ListItemText primary="Settings"></ListItemText>
            </MenuItem>
            <MenuItem onClick={() => { this.props.logOut(); this.handleClose('/') }}>
              <ListItemText primary="Logout"></ListItemText>
            </MenuItem>
          </Menu>
          </ListItem>
          <ListItem>
            {props.currentProfile &&
              <Select key="language"
                      onChange={handleChangeLanguage}
                      value={props.currentLanguage || 'none'}
                      input={<Input name="age" id="age-simple" color="contrast"/>}>
                { props.currentProfile.languages.map(lang =>
                  <MenuItem key={lang} value={lang}>{languages[lang]}</MenuItem>)
                  }
                { props.currentProfile.native && 
                  <MenuItem value={props.currentProfile.native}>{languages[props.currentProfile.native]} (native)</MenuItem>}
              </Select>
              }
          </ListItem>
        </List>
        <Button aria-owns={open ? 'menu-appbar' : null}
            aria-haspopup="true"
            onClick={handleMenu}>
          <i className="ion-compose"></i>&nbsp;...
        </Button>
      </React.Fragment>);
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoggedInView))