import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as form } from 'redux-form'

import common from './reducers/common';
import editor from './reducers/editor';
import home from './reducers/home';
import profile from './reducers/profile';
import settings from './reducers/settings';
import threadTabs from './reducers/threadTabs';
import threadList from './reducers/threadList';
import auth from './reducers/auth';

export default combineReducers({
  threadList,
  threadTabs,
  auth,
  common,
  editor,
  home,
  profile,
  settings,
  form,
  routing: routerReducer,
});
