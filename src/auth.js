import { LOAD_MY_PROFILE } from './constants/actionTypes';
import { store } from './store'
import { agent } from 'langchat-client'

export default {
  setToken: (_token) => {
    agent.setToken(_token)
    store.dispatch({ type: LOAD_MY_PROFILE, payload: agent.Profile.me() });
  },
}