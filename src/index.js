import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import { createMuiTheme, MuiThemeProvider } from 'material-ui';

import { store, history } from './store';
import App from './components/App';
import Home from './components/Home'
import Login from './components/Login'

require('./css/style.scss')
require('../assets/favicon.ico')

const theme = createMuiTheme({
  overrides: {
    MuiPaper: {
      root: {

      }
    }
  }
});

ReactDOM.render((
  <Provider store={store}>
    <MuiThemeProvider theme={theme}>
      <ConnectedRouter history={history}>
        <Switch>
          <Route path="/" component={App} />
        </Switch>
      </ConnectedRouter>
    </MuiThemeProvider>
  </Provider>
), document.getElementById('root')
);
