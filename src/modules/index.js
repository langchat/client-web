import Chat from './Chat'
import MakeSentence from './MakeSentence'
import Quiz from './Quiz'
//import ReadingChallenge from 'reading-challenge-client'

export default [
  {
    slug: 'chat',
    title: 'Chat',
    component: Chat,
  },
  /*
  {
    slug: 'reading-challenge',
    title: 'Reading Challenge',
    component: ReadingChallenge,
  },
  {
    slug: 'make-sentence',
    title: 'Make Sentence',
    component: MakeSentence,
  },
  {
    slug: 'quiz',
    title: 'Quiz',
    component: Quiz,
  },
  */
]
