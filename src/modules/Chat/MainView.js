import React from 'react'
import { agent } from 'langchat-client'
import { connect } from 'react-redux'

import ThreadList from './ThreadList';
import { CHANGE_TAB } from '../../constants/actionTypes';
import ThreadTabView from './ThreadTabView';
import { Grid } from 'material-ui';
import RightSideBar from './RightSideBar'

const styles = {
  leftPane: {

  },
}

const mapStateToProps = state => ({
  ...state,
});

const mapDispatchToProps = dispatch => ({
  onTabClick: (tab, pager, payload) => dispatch({
    type: CHANGE_TAB, tab, pager, payload,
  }),
});

const MainView = props => (
  <div style={{ height: '100%' }}>
    <ThreadTabView />
  </div>
);

export default connect(mapStateToProps, mapDispatchToProps)(MainView);
