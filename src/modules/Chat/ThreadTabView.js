import React from 'react';
import { ARTICLE_UNFAVORITED, CHANGE_THREAD_TAB, CLOSE_THREAD_TAB } from '../../constants/actionTypes';
import { agent } from 'langchat-client'
import { connect } from 'react-redux';
import Thread from './Thread';
import { AppBar, BottomNavigation, BottomNavigationAction, Tab, Tabs, TabContainer, Button, TextField, Select } from 'material-ui';
import SwipeableViews from 'react-swipeable-views'
import ThreadDefaultTab from './ThreadDefaultTab';

const mapDispatchToProps = dispatch => ({
  close: thread => dispatch({
    type: CLOSE_THREAD_TAB,
    thread,
  }),
  change: thread => dispatch({ type: CHANGE_THREAD_TAB, thread }),
});

const mapStateToProps = state => ({
  ...state,
  threads: state.threadTabs.threads,
  currentThread: state.threadTabs.currentThread,
  currentThreadIndex: state.threadTabs.currentThreadIndex,
});

const styles = {
  root: {
    height: '100%',
    display: 'flex',
    flexFlow: 'column',
  },
}

class ThreadTabView extends React.Component {
  getThread(id) {
    return this.props.threads[id - 1]
  }

  render() {
    const { props, state } = this

    const handleTabChange = (e, id) => {
      props.change(this.getThread(id));
    }

    const handleChangeIndex = (id) => {
      // props.change(this.getThread(id));
    }

    return (
      <div style={styles.root}>
        <AppBar position="static" color="default">
          <Tabs value={props.currentThreadIndex} onChange={handleTabChange}>
            <Tab label="Default" />
            {
            props.threads.map(thread => <Tab label={thread.title} key={thread.id} />)
          }
          </Tabs>
        </AppBar>
        <div style={{ height: 'calc(100% - 48px)' }}>
          <SwipeableViews
            index={props.currentThreadIndex}
            onChangeIndex={handleChangeIndex}
            style={{ height: '100%' }}
          >
            <ThreadDefaultTab />
            { props.threads.map(thread => <Thread key={thread.id} thread={thread} />) }
          </SwipeableViews>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ThreadTabView);
