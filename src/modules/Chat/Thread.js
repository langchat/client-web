import Message from './Message';
import React from 'react';
import * as ACTION from '../../constants/actionTypes';
import { connect } from 'react-redux';
import { agent } from 'langchat-client'
import ThreadSendMsgForm from './ThreadSendMsgForm';
import { Divider } from 'material-ui'

const mapStateToProps = state => ({ ...state,});

const mapDispatchToProps = dispatch => ({
  addMessage: msg => dispatch({ type: ACTION.MESSAGE_ADDED, msg }),
  send: (tid, msg) => dispatch({ type: ACTION.SEND_MESSAGE, payload: agent.Messages.send(tid, msg) }),
});

class Thread extends React.Component {
  constructor(props) {
    super(props);
    this.state = { messages: [] }
    agent.Messages.onNewMsg(this.props.thread.id, (msg) => {
      this.setState({ messages: this.state.messages.concat([msg]) });
      this.scrollToBottom()
    });

    agent.Messages.all(this.props.thread.id)
      .then((ret) => { this.setState({ messages: ret.data }) })
  }

  componentWillMount() {

  }

  componentDidMount() {
  }

  scrollToBottom() {
    this.messagesEnd.scrollIntoView({ behavior: 'smooth' });
  }

  render() {
    const { props, state } = this
    let content

    if (!state.messages) content = <div className="article-preview">Loading...</div>
    else if (state.messages.length === 0) {
      content = (<div className="article-preview">No messages are here... yet.</div>)
    } else {
      content = (<div>
        {
          state.messages.map(message => {
            return (
              <Message message={message} user={props.thread.users.find(u => u.id === message.from)} key={message.id} />
            );
          })
        }
        <div style={{ float:"left", clear: "both" }}
             ref={(el) => { this.messagesEnd = el; }}>
        </div>
      </div>)
    }

    function handleSubmit(values) {
      props.send(props.thread.id, values.content);
      //agent.Messages.send(props.thread.id, values.content)
    }

    return (
      <div style={styles.root}>
        <div style={styles.messageContainer}>
          {content}
        </div>
        <Divider />
        <ThreadSendMsgForm onSubmit={handleSubmit} />
      </div>
    )
  }
}


const styles = {
  root: {
    height: '100%',
    display: 'flex',
    flexFlow: 'column',
  },
  messageContainer: {
    flex: 1,
    overflow: 'auto',
  },
}

export default connect(mapStateToProps, mapDispatchToProps)(Thread);
