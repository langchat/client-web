import React from 'react';
import { agent } from 'langchat-client'

const Friends = props => {
  const friends = props.friends;
  if (friends) {
    return (
      <div className="tag-list">
        {
          friends.map(friend => {
            const handleClick = ev => {
              ev.preventDefault();
              //props.onClickTag(friend, page => agent.Articles.byTag(tag, page), agent.Articles.byTag(tag));
            };

            return (
              <a
                href="#"
                className="tag-default tag-pill"
                key={friend}
                onClick={handleClick}>
                {friend.username}
              </a>
            );
          })
        }
      </div>
    );
  } else {
    return (
      <div>Loading Friends...</div>
    );
  }
};

export default Friends;
