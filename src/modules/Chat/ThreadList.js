import ThreadPreview from './ThreadPreview';
import ListPagination from './ListPagination';
import React from 'react';
import { ARTICLE_FAVORITED, ARTICLE_UNFAVORITED, OPEN_THREAD_TAB } from '../../constants/actionTypes';
import { agent } from 'langchat-client'
import { connect } from 'react-redux';
import { Drawer, List, ListItem, ListItemText, ListItemAvatar, Divider, Avatar, ListSubheader } from 'material-ui';

const mapDispatchToProps = dispatch => ({
  open: thread => dispatch({ type: OPEN_THREAD_TAB, thread }),
});

const ThreadList = (props) => {
  if (!props.threads) {
    return (
      <div className="article-preview">Loading...</div>
    );
  }

  if (props.threads.length === 0) {
    return (
      <div className="article-preview">
        No threads are here... yet.
      </div>
    );
  }

  const handleClick = (e, thread) => {
    e.preventDefault();
    props.open(thread);
  }

  return (
    <List>
      <ListItem button>
        <ListItemAvatar><Avatar /></ListItemAvatar>
        <ListItemText primary="Someone" />
      </ListItem>
      <ListSubheader>Favorite</ListSubheader>
      <ListSubheader>Threads</ListSubheader>
      {
        props.threads.map(thread => (
          <ListItem button onClick={e => handleClick(e, thread)} key={thread.id}>
            <ListItemAvatar><Avatar /></ListItemAvatar>
            <ListItemText primary={thread.title} />
          </ListItem>
          ))
      }
    </List>
  );
};

export default connect(() => ({}), mapDispatchToProps)(ThreadList);
