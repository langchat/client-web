import React from 'react';
import { ARTICLE_UNFAVORITED, CHANGE_THREAD_TAB, CLOSE_THREAD_TAB,
  OPEN_THREAD_WITH_USER,
  OPEN_THREAD_TAB,
} from '../../constants/actionTypes';
import { agent } from 'langchat-client'
import { connect } from 'react-redux';
import Thread from './Thread';
import ThreadList from './ThreadList';
import { Card, CardContent, CardActions, CardMedia, Typography,
  AppBar, Paper, BottomNavigation, BottomNavigationAction, Tab, Tabs, Button, TextField,
  Select, Input, MenuItem, Grid, CircularProgress } from 'material-ui';
import SwipeableViews from 'react-swipeable-views'
import GridList, { GridListTile, GridListTileBar } from 'material-ui/GridList';
import { withStyles } from 'material-ui/styles';

const languages = require('../../constants/languages')

const mapDispatchToProps = dispatch => ({
  openUser: user => agent.Threads.getByUser(user).then(thread => dispatch({
    type: OPEN_THREAD_TAB,
    thread,
  })),
  openThread: thread => dispatch({ type: OPEN_THREAD_TAB, thread }),
});

const mapStateToProps = state => ({
  ...state,
  currentProfile: state.common.currentProfile,
  currentLanguage: state.common.currentLanguage,
  threads: state.threadList.threads,
});

const cardHeight = 200
const styles = theme => ({
  gridList: {
    height: 'calc(100% - 48px)',
    padding: theme.spacing.unit * 2,
  },
  card: {
    margin: theme.spacing.unit,
  },
})

class ThreadDefaultTab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searching: false,
      users: [],
      threads: [],
    }

    agent.Threads.onNewRequest((data) => {
      if (this.state.users.find(u => u.id === data.id)) return
      this.setState({ users: this.state.users.concat([data]) })
    })
  }

  render() {
    const { props, state } = this

    const handleFind = (e) => {
      if (this.state.searching) {
        agent.Threads.stopRequest()
      } else {
        agent.Threads.startRequest()
      }
      this.setState({ searching: !this.state.searching })
    }

    const handleChat = (user) => {
      this.props.openUser(user)
    }

    const handleClick = (e, thread) => {
      e.preventDefault();
      props.openThread(thread);
    }

    const loadThreads = () => {
      agent.Threads.all().then(res => this.setState({ threads: res.data }))
    }

    loadThreads()

    const { classes } = props

    return (
      <GridList cellHeight={cardHeight} className={classes.gridList} cols={4}>
        <GridListTile cols={2} rows={2}>
          <Card>
            <CardContent>

            </CardContent>
          </Card>
          </GridListTile>
          {
            state.threads.map(thread => (
              <GridListTile key={thread.id}>
              <Card className={classes.card}>
                <CardContent>
                <Typography type="headline">{thread.title}</Typography>
                </CardContent>
                <CardActions>
          <Button onClick={e => handleClick(e, thread)} dense>Open Chat</Button>
        </CardActions>
              </Card>
              </GridListTile>
              ))
          }

      </GridList>

      /*
      <Grid container>
        <Grid xs={6} item>
        <Paper className={classes.paper}>
          <ThreadList
            pager={props.pager}
            threads={props.threads}
            loading={props.loading}
            threadCount={props.threadCount}
            currentPage={props.currentPage}
          /></Paper>
        </Grid>
        <Grid xs={6} item><Paper className={classes.paper}>
          <Typography type="subheading">I want to speak</Typography>
          <Select
            key="language"
            onChange={handleChangeLanguage}
            value={state.language}
            input={<Input name="age" id="age-simple" color="contrast" />}
          >
            {props.currentProfile.languages.map(lang =>
                <MenuItem key={lang} value={lang}>{languages[lang]}</MenuItem>)}
            <MenuItem value={props.currentProfile.native}>{languages[props.currentProfile.native]} (native)</MenuItem>
          </Select>
          <GridList cols={2.5}>
            { state.users.map(user => (
                <GridListTile key={user.id}>
                  <Card>
                    <CardMedia
                      image={user.avatar}
                      title={user.username}
                    />
                    <CardContent>
                      <Typography type="headline" component="h2">
                        {user.username}
                      </Typography>
                      <Typography component="p">
                      Native in {languages[user.native]}
                      </Typography>
                    </CardContent>
                    <CardActions>
                      <Button dense color="primary" onClick={() => handleChat(user)}>
                      Chat
                      </Button>
                    </CardActions>
                  </Card>
                </GridListTile>
        )) }
          </GridList>
          { state.searching && <Grid xs={3} item>
            <Card key={0}>
                <CardContent>
                  <CircularProgress />
                </CardContent>
              </Card>
                               </Grid>
                               
        }

<CardActions>
          <Button raised color="primary" onClick={handleFind}>{ state.searching ? 'Stop' : 'Find Me Someone' }</Button>
        </CardActions>
        </Paper>
        </Grid>
      </Grid>*/
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(ThreadDefaultTab))
