import React from 'react';
import { connect } from 'react-redux';
import { Grid } from 'material-ui';

import MainView from './MainView';
import Friends from './Friends';
import { agent } from 'langchat-client'
import {
  HOME_PAGE_LOADED,
  HOME_PAGE_UNLOADED,
  APPLY_TAG_FILTER
} from '../../constants/actionTypes';

const mapStateToProps = state => ({
  ...state.home,
  appName: state.common.appName,
  token: state.common.token,
});

const mapDispatchToProps = dispatch => ({
  onClickTag: (tag, pager, payload) => dispatch({
    type: APPLY_TAG_FILTER,
    tag,
    pager,
    payload,
  }),
  onLoad: payload => dispatch({ type: HOME_PAGE_LOADED, payload }),
  onUnload: () => dispatch({ type: HOME_PAGE_UNLOADED }),
});

const styles = {
  root: {
    height: '100%',
  },
}

class Chat extends React.Component {
  componentWillMount() {
    this.props.onLoad(Promise.all([agent.Friends.all(), agent.Threads.all()]));
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    return (
      <div style={styles.root}>
        <MainView />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
