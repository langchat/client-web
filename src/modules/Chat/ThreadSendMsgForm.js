import Message from './Message';
import React from 'react';
import * as ACTION from "../../constants/actionTypes";
import {connect} from "react-redux";
import { agent } from 'langchat-client'
import { reduxForm, Field } from 'redux-form';
import {TextField} from 'redux-form-material-ui';

const ThreadSendMsgForm = props => {
  const { handleSubmit, fields: {content} } = props

  const handleInput = e => {
    if (e.keyCode == 13 && e.shiftKey == false && (e.target.value !== "")) {
      handleSubmit();
      props.change("content", "");
      e.preventDefault();
    }
  }

  return (
    <div style={styles.root}>
    <form onSubmit={handleSubmit}>
      <Field name="content" component={TextField} onKeyDown={handleInput}
             placeholder="Type a message" style={styles.textArea}></Field>
    </form>
    </div>
  )
};

const styles = {
  root: {
    padding: 10,
    minHeight: 100
  },
  textArea: {
    width: '100%',
    height: '100%',
  }
}

export default reduxForm({ form: 'newMsg', fields: ['content'] })(ThreadSendMsgForm);