import React from 'react';
import { Link } from 'react-router-dom';
import { agent, Avatar } from 'langchat-client'
import { connect } from 'react-redux';
import { ARTICLE_FAVORITED, ARTICLE_UNFAVORITED } from '../../constants/actionTypes';
import { ListItem, ListItemText, ListItemAvatar, ListItemIcon, Paper } from 'material-ui';

console.log(Avatar)

const FAVORITED_CLASS = 'btn btn-sm btn-primary';
const NOT_FAVORITED_CLASS = 'btn btn-sm btn-outline-primary';

const mapDispatchToProps = dispatch => ({
  favorite: slug => dispatch({
    type: ARTICLE_FAVORITED,
    payload: agent.Articles.favorite(slug),
  }),
  unfavorite: slug => dispatch({
    type: ARTICLE_UNFAVORITED,
    payload: agent.Articles.unfavorite(slug),
  }),
});

const mapStateToProps = state => ({
  currentUser: state.common.currentUser,
  currentUserId: state.common.currentUser.id,
});

const Message = (props) => {
  const { message } = props;

  const handleClick = (ev) => {
    // ev.preventDefault();
    // if (article.favorited) {
    //   props.unfavorite(article.slug);
    // } else {
    //   props.favorite(article.slug);
    // }
  };

  const style = props.currentUserId == props.message.from ? styles.left : styles.right;
  return (
    <ListItem>
      { props.currentUserId === props.message.from ? (<div style={style}>
        <div style={{ float: 'left' }}>
        <Avatar user={props.currentUser} />
      </div>
        <div style={style.text}>
          <Paper style={styles.chatbox}>
          <ListItemText secondary={message.content} />
        </Paper>
        </div>
      </div>) :
        (<div style={style}>
          <div style={{ float: 'right' }}>
          <Avatar user={props.currentUser} />
        </div>
          <div style={style.text}>
            <Paper style={styles.chatbox}>
            <ListItemText secondary={message.content} />
          </Paper>
          </div>
         </div>)
      }
    </ListItem>
  );
}

const styles = {
  left: {
    text: {
      float: 'left',
    },
    width: '100%',
    alignLeft: true,
  },
  right: {
    text: {
      float: 'right',
    },
    width: '100%',
    alignRight: true,
  },
  chatbox: {
    padding: 10,
    marginLeft: 10,
    marginRight: 10,
    maxWidth: '80%',
  },
}

export default connect(mapStateToProps, mapDispatchToProps)(Message);
