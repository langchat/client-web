import React from 'react';
import {Button, Card, CardActions, CardContent, Typography, withStyles, LinearProgress} from "material-ui";

const styles = theme => ({
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    marginBottom: 16,
    fontSize: 14,
    color: theme.palette.text.secondary,
  },
  pos: {
    marginBottom: 12,
    color: theme.palette.text.secondary,
  },
});

class LevelWidget extends React.Component {
  render() {
    const props = this.props
    const { classes } = props
    return (
      <Card className={classes.card}>
        <CardContent>
          <Typography type="headline" component="h2">
            Your Level
          </Typography>
          <LinearProgress mode="buffer" value={50} valueBuffer={60}></LinearProgress>
          <Typography component="p" style={{marginTop: 15}}>
            Keep chatting to improve your skill!
          </Typography>
        </CardContent>
        <CardActions>
          <Button dense>Learn More</Button>
        </CardActions>
      </Card>
    )
  }
}

export default withStyles(styles)(LevelWidget);