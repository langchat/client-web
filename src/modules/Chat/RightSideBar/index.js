import React from 'react';
import LevelWidget from "./LevelWidget";

class RightSideBar extends React.Component {
  render() {
    return (<div style={{paddingRight: 10}}>
      <LevelWidget/>
    </div>)
  }
}

export default RightSideBar;