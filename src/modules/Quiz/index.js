import React from 'react';
import { connect } from 'react-redux';
import { Grid } from 'material-ui';
import { agent } from 'langchat-client'
import {
} from '../../constants/actionTypes';

const mapStateToProps = state => ({
  ...state.home,
  appName: state.common.appName,
  token: state.common.token,
});

const mapDispatchToProps = dispatch => ({
  
});

const styles = {
  root: {
    height: '100%',
  },
}

class Quiz extends React.Component {
  componentWillMount() {
    //this.props.onLoad(Promise.all([agent.Friends.all(), agent.Threads.all()]));
  }

  componentWillUnmount() {
    //this.props.onUnload();
  }

  render() {
    return (
      <div style={styles.root}>
        
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Quiz);
