import {
  ARTICLE_FAVORITED,
  ARTICLE_UNFAVORITED,
  SET_PAGE,
  APPLY_TAG_FILTER,
  HOME_PAGE_LOADED,
  HOME_PAGE_UNLOADED,
  CHANGE_TAB,
  PROFILE_PAGE_LOADED,
  PROFILE_PAGE_UNLOADED,
  PROFILE_FAVORITES_PAGE_LOADED,
  PROFILE_FAVORITES_PAGE_UNLOADED, OPEN_THREAD_TAB, CLOSE_THREAD_TAB, APP_LOAD, CHANGE_THREAD_TAB, MESSAGE_LOADED,
  MESSAGE_ADDED,
  OPEN_THREAD_WITH_USER
} from '../constants/actionTypes';

export default (state = {}, action) => {
  console.log(action)
  switch (action.type) {
    case APP_LOAD:
      return {...state, threads: [], currentThread: null }
    case OPEN_THREAD_TAB: 
      let thread = state.threads.find((t) => action.thread.id === t.id)
      if (thread) {
        return {
          ...state,
          currentThread: thread,
          currentThreadIndex: state.threads.indexOf(thread) + 1,
        }
      } else {
        var newThread = { ...action.thread }
        state.threads.push(newThread)
        return {...state,
          currentThreadIndex: state.threads.length,
          currentThread: newThread
        }
      }
    case CHANGE_THREAD_TAB:
      return {...state,
        currentThread: action.thread,
        currentThreadIndex: state.threads.indexOf(action.thread) + 1,
      }
    case CLOSE_THREAD_TAB:
      state.threads.splice(state.threads.indexOf(action.thread));
      return state;
    default:
      return state;
  }
};
